<!DOCTYPE html>
<html>
<head>
	<title>Inscription Admin</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/adminreg.css">
</head>
<body>
	<div class="box">
		<form method="post" action="Back/verif-admin-inscri.php">
			<h1>INSCRIPTION</h1>
			<table>
				<tr>
						<td id="col-label"><label id="label">Votre nom :</label></td>
						<td><input id="champ" type="text" placeholder="Entrer votre nom" name="nom"></td>
				</tr>
				<tr>
						<td id="col-label"><label id="label">Votre prenom :</label></td>
						<td><input id="champ" type="text" placeholder="Entrer votre prenom" name="prenom"></td>
				</tr>
				<tr>
						<td id="col-label"><label id="label">Votre adresse email :</label></td>
						<td><input id="champ" type="text" value="@gmail.com" name="mail"></td>
				</tr>
				<tr>
						<td id="col-label"><label id="label">Votre mot de passe :</label></td>
						<td><input id="champ" type="password" placeholder="Entrer votre mot de passe" name="mdp"></td>
				</tr>
			</table>

			<button id="register-btn" type="submit">S'inscrire</button> 
		</form>
		<?php if (isset($_GET['m'])) : ?>
			<div class="flash-data" data-flashdata="<?= $_GET['m']; ?>"></div>
		<?php endif; ?>
	</div>
	<script type="text/javascript" src="sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="jquery-3.5.1.min.js"></script>
	<script>
		const flashdata = $('.flash-data').data('flashdata')
		if (flashdata) {
			Swal.fire({
				icon: 'error',
				title: 'Inscription echouée',
				text: 'S\'il vous plait, veuillez remplir les champs'
			})
		}
	</script>
</body>
</html>