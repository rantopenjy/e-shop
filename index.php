<!DOCTYPE html>
<html>
<head>
	<title>e-Shop</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="CSS/index.css">
</head>
<body>
	<div class="admin">
		 <a href="Front/adminlog.php" title="Admin Login"><span style="font-size: 30px;">►</span></a>
	</div>
	<form action="Front/connexion.php">
		<h1>e-Shop<sup><span style="font-size: 15px">©</span></sup></h1>
		<p>Bienvenue sur le site de commerce en ligne <span style="font-weight: bold;">e-Shop</span><br>
		Veuillez vous connecter pour faire vos achats</p>
		<input type="submit" value="Se connecter">
		<a href="Front/register.php"><i>Pas de compte? Créer un compte</i></a>
	</form>
</body>
</html>