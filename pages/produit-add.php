<div class="container">

	<div class="row">
		<div class="col-lg-12">
			<div class="d-sm-flex align-items-center justify-content-between mb-4">
	            <h1 class="h3 mb-0 text-gray-800">Les produits disponibles:</h1>
	            <a href="main-admin.php?page=ajout" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Ajouter un produit</a>
	        </div>
         </div>

		<?php 
			while ($article=$articles->fetch()) {
			$imgs=$bdd->query('SELECT * FROM images WHERE id_article='.$article['id_article'].' limit 0,1');
			$img=$imgs->fetch();
		?>

		<div class="col-lg-4 col-md-6">

			<div class="card shadow mb-4" style="height: 400px;">

				<div class="card-header py-2" style="position: relative;">
					<h5 class="m-0 font-weight-bold text-primary text-center"><?php echo $article['nom_article'];?></h5>
				</div>

				<div class="card-body" style="display: flex; align-items: center; justify-content: space-around;">
						<?php echo '<img style="" src="../uploads/'.$img['nom_image'].'" width="200px">'; ?>
				</div>

				<div class="bg-white">

					<div style="padding-top: 10px">
						<h6 class="text-center"><?php echo $article['descri'];?></h6>
						<h4 class="text-center"><?php echo $article['prix'];?> Ariary</h4>
					</div>

				</div>

				<div class="card-footer" style="display: flex; justify-content: space-around;">
					<?php echo '
					<a data-toggle="modal" data-target="#modal2'.$article['id_article'].'" href="#" class="btn btn-primary btn-icon-split">
	                    <span class="icon text-white-50">
	                      <i class="fas fa-image"></i>
	                    </span>
	                    <span class="text">Changer</span>
                  	</a>
					<a data-toggle="modal" data-target="#modal1'.$article['id_article'].'" href="#" class="btn btn-danger btn-icon-split">
	                    <span class="icon text-white-50">
	                      <i class="fas fa-trash"></i>
	                    </span>
	                    <span class="text">Supprimer</span>
                  	</a>';
		 			?>
		 		</div>

			</div>

		</div>

		<?php 
	 	echo '<div class="modal fade" id="modal1'.$article['id_article'].'" role="dialog">
                <div class="modal-dialog modal-xs">
		            
		            <div class="modal-content">

		                <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal">&times;</button>
			                <h3></h3>
			           	</div>

		                <div class="modal-body">
			                Voulez-vous vraiment supprimer ce produit?
			            </div>

		                <div class="modal-footer">
		                    	<a class="btn btn-danger" href="../Back/suppr-article.php?id='.$article['id_article'].'">Supprimer le produit</a>
		                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
		                </div>

	                </div>

                </div>
            </div>

            <div class="modal fade" id="modal2'.$article['id_article'].'" role="dialog">
                <div class="modal-dialog modal-xs">
		            
		            <div class="modal-content">

		                <div class="modal-header">
		                	<h4>Changer l\'image </h4>

			                <button type="button" class="close" data-dismiss="modal">&times;</button>
			                <h3></h3>
			           	</div>

		                <div class="modal-body">
		                    <form method="post" action="../Back/cible.php" enctype="multipart/form-data">

		                    <img src="" id="blah">

		                	<input type="file" accept="image/* name="photo" id="input" onchange="readURL(this);">
			            </div>

		                <div class="modal-footer">
		                    	<button type="submit" class="btn btn-primary" id="btn">Changer l\'image</button>
		                    </form>
		                    <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
		                </div>

	                </div>

                </div>
            </div>';

		?>			
	
	<?php } ?>

	<?php if (isset($_GET['m'])) : ?>
		<div class="flash-data" data-flashdata="<?= $_GET['m']; ?>"></div>
	<?php endif; ?>

	</div>

</div>

