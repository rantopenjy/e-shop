<div class="container">

	<div class="row">
		<div class="col-lg-12">
			<div class="d-sm-flex align-items-center justify-content-between mb-4">
	            <h1 class="h3 mb-0 text-gray-800">Ajout de produit:</h1>
	        </div>
        </div>
	</div>

	<div class="card shadow">
		<div class="row">
			<div class="col-lg-5">
				<div class="card-body py-3">
					<form class="user" method="post" action="../Back/ajout-produit.php"  enctype="multipart/form-data">
						<div class="form-group">
							<label class="text-gray-800">Nom du produit:</label>
							<input type="text" class="form-control" name="article" required="">
						</div>
						<div class="form-group">
							<label class="text-gray-800">Description:</label>
							<textarea class="form-control" name="descri" required=""></textarea>
						</div>
						<div class="form-group">
							<label class="text-gray-800">Prix (en Ariary):</label>
							<input type="number" value="5000" class="form-control" name="prix" required="">
						</div>

						<div class="form-group">
							<label class="text-gray-800">Catégorie:</label>
							<select name="categ" class="form-control" required="">
								<?php 

									$bdd = new PDO('mysql:host=localhost;dbname=ecomm','root','');
									$req = $bdd->query('SELECT * from categ');

									while($reqs=$req->fetch()) {
										echo'<option value="'.$reqs['id_catg'].'">'.$reqs['nom_catg'].'</option>';
									}

								 ?>
							</select>
						</div>

					
				</div>
			</div>
			<div class="col-lg-7">
				<div class="card-body py-3">
					<div class="preview-box">
						<div class="image">
							<img id="blah" src="">
						</div>
						<div class="content">
							<i id="cloud-icon" class="fas fa-image"></i>
						</div>
						<input type="file" name="photo" onchange="readURL(this);" id="input" hidden="" required="">
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-lg-5">
						<button class="btn btn-primary" type="submit">Ajouter le produit</button>
					</form>
				</div>
				<div class="col-lg-7" style="display: flex;justify-content: center;">
					<button class="btn btn-primary" id="btn-upload" onclick="defaultBtnActive()">Choisir une image</button>
				</div>
			</div>
		</div>
	</div>	

</div>