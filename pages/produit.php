<div class="container">

	<div class="row">
		<div class="col-lg-12">
			<div class="d-sm-flex align-items-center justify-content-between mb-4">
	            <h1 class="h3 mb-0 text-gray-800">Tous nos produits disponibles:</h1>
	        </div>
         </div>
	</div>

	<div class="row">

		<?php 
			while ($article=$articles->fetch()) {
			$imgs=$bdd->query('SELECT * FROM images WHERE id_article='.$article['id_article'].' limit 0,1');
			$img=$imgs->fetch();
		?>

		<div class="col-lg-4 col-md-6">

			<div class="card shadow mb-4" style="height: 400px;">

				<div class="card-header py-2" style="position: relative;">
					<h5 class="m-0 font-weight-bold text-primary text-center"><?php echo $article['nom_article'];?></h5>
				</div>

				<div class="card-body" style="display: flex; align-items: center; justify-content: space-around; width: 100%">
						<?php echo '<img src="../uploads/'.$img['nom_image'].'" style="max-width:100%; max-height:100%; border-radius:5px">'; ?>
				</div>

				<div class="bg-white">

				<div style="padding-top: 10px">
					<h6 class="text-center"><?php echo $article['descri'];?></h6>
					<h4 class="text-center"><?php echo $article['prix'];?> Ariary</h4>
				</div>

				</div>

				<div class="card-footer" style="display: flex; justify-content: space-around;">
					<?php echo '
					<a data-toggle="modal" data-target="#modal1'.$article['id_article'].'" href="" class="btn btn-primary btn-icon-split"  '.$d.' >
						<span class="icon text-white-50">
	                      <i class="fas fa-dollar-sign"></i>
	                    </span>
	                    <span class="text">Commander</span>
					</a>';
		 			?>
		 		</div>

			</div>

		</div>

		<?php 
	 	echo '<div class="modal fade" id="modal1'.$article['id_article'].'" role="dialog">
                <div class="modal-dialog modal-xs">
		            
		            <div class="modal-content">

		                <div class="modal-header">
			                <h3>'.$article['nom_article'].'</h3>
			                <button type="button" class="close" data-dismiss="modal">&times;</button>
			           	</div>

		                <div class="modal-body">
			                <form action="../Back/commande.php" method="post">
				                <div class="form-group">
				                    <label>Quantité</label>
				                    <input type="hidden" value="'.$article['id_article'].'" name="id">
				                    <input class="form-control" type="number" name="qtt">
				                </div>
			            </div>

		                <div class="modal-footer"> 
		                    <button type="submit" class="btn btn-primary">Valider la commande</button>
		                	</form>
		                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
		                </div>

	                </div>

                </div>
            </div>';
	 ?>			
	
	<?php } ?>

	</div>
</div>