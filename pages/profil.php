<?php 

	$clt=$bdd->query('SELECT * FROM client where id_clt='.$_SESSION['actif'].'');
	$clts=$clt->fetch();

 ?>

<div class="container">

	<div class="card shadow">
		<div class="row">
			<div class="col-lg-5">
				<div class="card-body py-3">
					<div class="profilBox1">
						<div class="imgBox">
							<?php 
								echo '<img src="../uploads/'.$clts['img_clt'].'">';
							 ?>
						</div>
						<a id="importBtn" href="#" class="btn btn-primary btn-circle">
                    		<i class="fas fa-image"></i>
                		</a>
						<h1 class="h3 mb-0 text-gray-800"><span><?php echo $clts['nom_clt'].'</span>'; ?></h1>
						<h1 class="h3 mb-0 text-gray-800"><?php echo $clts['prenom_clt'].'</span>'; ?></h1>
						<a href="#" class="btn btn-primary btn-circle">
                    		<i class="fas fa-cog"></i>
                		</a>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				
			</div>
		</div>

		<div class="col-lg-6">
			<div class="card-body py-3">					
			
			</div>
		</div>
		
	</div>

</div>
