<!DOCTYPE html>
<html>
<head>
	<title>E-Shop Login</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../CSS/connex.css">
</head>
<body>
	<div class="box">
		<form method="post" action="../Back/verifAdmin.php">
			<h1>Admin LOGIN</h1>
			<label>Mail</label>
			<input id="mail" type="text" placeholder="Entrer votre adresse mail" name="mail" />
			<label>Mot de passe</label>
			<input id="pass" type="password" placeholder="Entrer votre mot de passe" name="mdp" />
			<button id="btn" type="submit">Connexion</button>
			<a id="forgot" href="a"><i>Vous avez oublié votre mot de passe?</i></a>
		</form>
		<?php if (isset($_GET['m'])) : ?>
			<div class="flash-data" data-flashdata="<?= $_GET['m']; ?>"></div>
		<?php endif; ?>
		<?php if (isset($_GET['s'])) : ?>
			<div class="flash" data-flashdata="<?= $_GET['s']; ?>"></div>
		<?php endif; ?>
	</div>
	<script src="../jquery-3.5.1.min.js"></script>
	<script src="../sweetalert2.all.min.js"></script>
	<script>
		const flashdata = $('.flash-data').data('flashdata')
		if (flashdata) {
			Swal.fire({
				icon: 'error',
				title: 'Connexion echoué',
				text: 'S\'il vous plait, veuillez remplir les champs'
			})
		}

		const flash = $('.flash').data('flashdata')
		if (flash) {
			Swal.fire({
				icon: 'error',
				title: 'Erreur',
				text: 'Adresse mail ou mot de passe incorrect'
			})
		}
	</script>
</body>
</html>