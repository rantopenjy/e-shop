<?php 

  include('../base.php');
  include('../requette.php');
  $clts = $bdd->query('SELECT * FROM client WHERE id_clt = '.$_SESSION['actif'].'');
    $clt = $clts->fetch();

 ?>


<!DOCTYPE html>
<html lang="frw">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>e-Shop</title>

  <!-- Custom fonts for this template-->
  <link href="../SBAdmin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../SBAdmin/css/sb-admin-2.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="../CSS/profil.css">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-dark topbar mb-4 static-top shadow" style="background-color: #0085FF">

          <div class="container">

            <div class="navbar-brand">
              <h1><a class="text-white" href="main.php" style="text-decoration: none;"><b>e-Shop</b><sup><small>&copy;</small></sup></a></h1>
            </div>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

              <!-- Nav Item - Alerts -->
              <li class="nav-item no-arrow mx-1">
                <a class="nav-link text-gray-60" href="main.php?page=accueil">Accueil
                </a>
              </li>

              <li class="nav-item no-arrow mx-1">
                <a class="nav-link text-gray-60" href="main.php?page=produit">Produits
                </a>
              </li>

              <li class="nav-item no-arrow mx-1">
                <a class="nav-link text-gray-60" href="main.php?page=categorie">Catégorie
                </a>
              </li>

              <li class="nav-item no-arrow mx-1">
                <a class="nav-link text-gray-60" href="main.php?page=arrivage">Arrivage
                </a>
              </li>              

              <div class="topbar-divider d-none d-sm-block"></div>

              <!-- Nav Item - User Information -->
              <?php echo '
              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2 d-none d-lg-inline text-gray-60">'.$clt['prenom_clt'].' '.$clt['nom_clt'].'</span>
                  <img style="object-fit: cover" class="img-profile rounded-circle" src="../uploads/'.$clt['img_clt'].'">
                </a>';
                 ?>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="main.php?page=profil">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profil
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Paramètres
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Déconnexion
                  </a>
                </div>
              </li>

            </ul>

            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
              <i class="fa fa-bars"></i>
            </button>

          </div>

        </nav>
        <!-- End of Topbar -->

      