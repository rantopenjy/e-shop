<?php 
	
	session_start();

 ?>


<!DOCTYPE html>
<html>
<head>
	<title>E-Shop Inscription</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../CSS/insertimg.css">
	<style type="text/css">
		
	</style>
</head>
<body>
	<div class="box">

		<form method="post" action="../Back/insertpdp.php" enctype="multipart/form-data">
			<h1>PHOTO DE PROFIL</h1>
			<p style="text-align: center; color: #777;">Veuillez inserer votre photo de profil</p>

			<div class="preview-box">
				<div class="image">
					<img id="blah" src="../uploads/default-profile.png">
				</div>

				<input type="button" id="import-btn" value="Choisir une photo" onclick="defaultBtnActive()">

			</div>

				<input type="file" id="input" accept="image/*" name="pdp" onchange="readURL(this);" hidden="">

				<button id="register-btn" type="submit" name="val">Valider la photo de profil</button>
			
		</form>

		<form method="post" action="insertimg.php">
			
			<button id="register-btn" type="submit" name="btn">Le faire plus tard</button>

		</form>
		
	</div>
	<script type="text/javascript" src="../sweetalert2.all.min.js"></script>
	<script type="text/javascript" src="../jquery-3.5.1.min.js"></script>
	<script>

		const flashdata = $('.flash-data').data('flashdata')
		if (flashdata) {
			Swal.fire({
				icon: 'error',
				title: 'Inscription echouée',
				text: 'S\'il vous plait, veuillez remplir les champs'
			})
		}

		const input = document.querySelector('#input');
    	const customBtn = document.querySelector('#import-btn');
    	const previewbox = document.querySelector('.preview-box');
    	const img = document.querySelector('#blah');

		function defaultBtnActive(){
      		input.click();
    	}

	    input.addEventListener('change', function(){
	  		const file = this.files[0];
	    	if(file){
	        	const reader = new FileReader();
	        	reader.onload = function(){
		          	const result = reader.result;
		          	img.src = result;
	      		}
	        reader.readAsDataURL(file);
	     	}
	    });

	</script>

	<?php 

	if(isset($_POST['btn'])){

			$bdd = new PDO('mysql:host=localhost;dbname=ecomm','root','');
			$req = $bdd->query('UPDATE client SET img_clt="default-profile.png" WHERE id_clt="'.$_SESSION['actif'].'" ');
			header('location:main.php?m=1');

	}

	 ?>
</body>
</html>