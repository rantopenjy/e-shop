/*
SQLyog Enterprise - MySQL GUI v8.1 
MySQL - 5.6.17 : Database - ecomm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`ecomm` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ecomm`;

/*Table structure for table `achat` */

DROP TABLE IF EXISTS `achat`;

CREATE TABLE `achat` (
  `id_article` int(11) DEFAULT NULL,
  `id_clt` int(11) DEFAULT NULL,
  `qtt_achat` int(11) DEFAULT NULL,
  `date_achat` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `nom_admin` varchar(255) DEFAULT NULL,
  `prenom_admin` varchar(255) DEFAULT NULL,
  `mail_admin` varchar(255) DEFAULT NULL,
  `mdp_admin` varchar(50) DEFAULT NULL,
  `img_admin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `article` */

DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `nom_article` varchar(255) DEFAULT NULL,
  `descri` varchar(3000) DEFAULT NULL,
  `prix` int(15) DEFAULT NULL,
  `id_catg` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Table structure for table `categ` */

DROP TABLE IF EXISTS `categ`;

CREATE TABLE `categ` (
  `id_catg` int(11) NOT NULL AUTO_INCREMENT,
  `nom_catg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_catg`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `client` */

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id_clt` int(11) NOT NULL AUTO_INCREMENT,
  `nom_clt` varchar(255) DEFAULT NULL,
  `prenom_clt` varchar(255) DEFAULT NULL,
  `mail_clt` varchar(255) DEFAULT NULL,
  `mdp_clt` varchar(50) DEFAULT NULL,
  `img_clt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_clt`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Table structure for table `images` */

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `nom_image` varchar(255) DEFAULT NULL,
  `id_article` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_image`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Table structure for table `manipulation` */

DROP TABLE IF EXISTS `manipulation`;

CREATE TABLE `manipulation` (
  `id_article` int(11) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
